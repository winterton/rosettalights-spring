package com.rosetta.lights.controllers;

import com.rosetta.lights.models.ColorRequestObject;
import com.rosetta.lights.models.ColorRequestQueue;
import com.rosetta.lights.services.ArduinoColorRequestService;
import com.rosetta.lights.tasks.TwitterQueueBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.util.LinkedList;

/**
 * Created by smithjw on 2/9/14.
 */
@Controller
@RequestMapping("/admin")
public class AdminController {

    @Autowired
    ColorRequestQueue<ColorRequestObject> queue;

    @Autowired
    ArduinoColorRequestService colorRequestService;

    @Autowired
    TwitterQueueBuilder builder;

    @RequestMapping(method = RequestMethod.GET)
    public ModelAndView printCurrentQueue(ModelAndView mav) {
        mav.setViewName("admin");

        if(queue != null) mav.addObject("queue",queue);
        mav.addObject("twitterStarted", builder.isListening());


        return mav;
    }

    @RequestMapping(value = "/refresh", method = RequestMethod.GET)
    public @ResponseBody
    LinkedList<ColorRequestObject> ajaxRefreshQueue()
    {
        return queue.getQueue();
    }
    
}
