package com.rosetta.lights.controllers;

import com.rosetta.lights.models.RosettaColor;
import com.rosetta.lights.models.RosettaColors;
import com.rosetta.lights.services.ColorMarshallService;
import org.omg.CORBA.Request;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.ListIterator;

@Controller
@RequestMapping("/colors")
public class ColorController {

    @Autowired
    private ColorMarshallService colorMarshallService;

    @RequestMapping(method = RequestMethod.GET, produces="application/json")
    public @ResponseBody
    RosettaColors getColorList()
    {

        RosettaColors colors = null;

        try {
            colors = colorMarshallService.getColors();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return colors;
    }

    @RequestMapping(method = RequestMethod.GET, value = "/get", params = {"hex"}, produces = "application/json")
    public @ResponseBody RosettaColor getColor(HttpServletRequest request, @RequestParam("hex") String hex)
    {
        hex = "#" + hex;
        if(hex != null) {
            RosettaColors colors = getColorList();

            for (RosettaColor color : colors.getColors()) {
                if (color.getHex().equalsIgnoreCase(hex)) {
                    return color;
                }
            }
        }

        return null;

    }

    @RequestMapping(method = RequestMethod.POST, value = "/add")
    public void addColorToList(HttpServletRequest request)
    {
        RosettaColors colors = colorMarshallService.getColors();

        String hex = "#" + request.getParameter("hex");


        Boolean found = false;
        if (hex != null) {
            for(RosettaColor color : colors.getColors()) {
                if (color.getHex().equalsIgnoreCase(hex)) {
                    found = true;
                    break;
                }
            }
        }

        if(found == false) {
            RosettaColor newColor = new RosettaColor();
            newColor.setHex(hex);
            newColor.setNames(new ArrayList<String>());
            colors.getColors().add(newColor);
            colorMarshallService.setColors(colors);
        }
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/delete")
    public void deleteColorFromList(HttpServletRequest request)
    {
        RosettaColors colors = colorMarshallService.getColors();

        final String hex = request.getParameter("hex");

        if (hex != null) {

            final ListIterator<RosettaColor> iterator = colors.getColors().listIterator();

            while (iterator.hasNext()) {
                final RosettaColor color = iterator.next();
                if (color.getHex().equalsIgnoreCase(hex)) {
                    iterator.remove();
                }
            }

            colorMarshallService.setColors(colors);
        }
    }

    @RequestMapping(method = RequestMethod.POST, value = "/save")
    public void saveNameToColor(HttpServletRequest request)
    {
        RosettaColors colors = colorMarshallService.getColors();

        final String hex = request.getParameter("hex");
        final String name = request.getParameter("name");
        final String prevName = request.getParameter("prevName");

        if (hex != null && name != null) {
            for (RosettaColor color : colors.getColors()) {
                if (color.getHex().equalsIgnoreCase(hex)) {
                    if(!color.getNames().contains(prevName) || StringUtils.isEmpty(prevName)) {
                        System.out.println("Adding " + name);
                        color.getNames().add(name.toUpperCase());
                    } else if(!StringUtils.isEmpty(prevName)) {
                        int index = color.getNames().indexOf(prevName);
                        color.getNames().remove(index);
                        color.getNames().add(name.toUpperCase());
                        System.out.println("Changing " + prevName + " to " + name);

                    }
                }
            }
            colorMarshallService.setColors(colors);
        }

    }

}
