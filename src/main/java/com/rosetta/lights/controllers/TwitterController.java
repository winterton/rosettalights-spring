package com.rosetta.lights.controllers;

import com.rosetta.lights.tasks.TwitterQueueBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.social.twitter.api.Tweet;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.List;

/**
 * Created by jsmith20 on 3/17/14.
 */
@Controller
@RequestMapping("/twitter")
public class TwitterController {

    @Autowired
    TwitterQueueBuilder twitterQueueBuilder;

    @RequestMapping(value = "/start", method = RequestMethod.POST)
    @ResponseStatus(value = HttpStatus.OK)
    public void startListeningToRequests()
    {
        System.out.println("Starting Twitter stream listener");
        twitterQueueBuilder.beginListening();
    }

    @RequestMapping(value = "/stop", method = RequestMethod.POST)
    @ResponseStatus(value = HttpStatus.OK)
    public void stopListeningToRequests()
    {
        System.out.println("Stopping Twitter stream listener");
        twitterQueueBuilder.stopListening();
    }

    @RequestMapping(value = "/mentions", method = RequestMethod.GET)
    public @ResponseBody
    List<Tweet> getMentions()
    {
        return twitterQueueBuilder.getTwitter().timelineOperations().getMentions(10);
    }
}
