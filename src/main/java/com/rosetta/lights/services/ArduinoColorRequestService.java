package com.rosetta.lights.services;

import com.rosetta.lights.models.ColorRequestObject;
import com.rosetta.lights.models.ColorRequestQueue;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.net.URL;
import java.util.logging.Logger;

/**
 * Created by smithjw on 2/9/14.
 */
@Service
public class ArduinoColorRequestService implements ColorRequestServiceInterface {

    @Autowired
    ColorRequestQueue<ColorRequestObject> queue;

    @Scheduled(fixedRate = 10000)
    public void processRequests() {
        ColorRequestObject colorRequest = null;

        if (!queue.isEmpty()) {
            colorRequest = queue.deque();
        }

        if (colorRequest != null) {

            RestTemplate template = new RestTemplate();
            String response = template.getForObject("http://10.4.37.38/arduino/r/{r}/g/{g}/b/{b}",
                    String.class,
                    colorRequest.getR(),
                    colorRequest.getG(),
                    colorRequest.getB());

            Logger.getLogger(ArduinoColorRequestService.class.toString()).info("REST Call Response: " + response);
        }
    }

}
