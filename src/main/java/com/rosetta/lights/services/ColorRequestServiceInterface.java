package com.rosetta.lights.services;

/**
 * Created by smithjw on 2/9/14.
 */
public interface ColorRequestServiceInterface {

    void processRequests();

}
