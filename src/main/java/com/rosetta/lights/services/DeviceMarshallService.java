package com.rosetta.lights.services;

import com.rosetta.lights.models.RosettaDevices;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by jsmith20 on 3/17/14.
 */
public class DeviceMarshallService {
    private static final String COLORS_FILE = "/colors.xml";
    private static final Logger log = Logger.getLogger(ColorMarshallService.class.getName());

   // private RosettaColors colors = null;

//    public RosettaColors getColors()
//    {
//        if(colors == null)
//        {
//            try(final InputStream colorsXml = getClass().getResourceAsStream(COLORS_FILE))
//            {
//
//                JAXBContext jxbContext = JAXBContext.newInstance(RosettaColors.class, RosettaColor.class);
//                Unmarshaller um = jxbContext.createUnmarshaller();
//
//                colors = (RosettaColors)um.unmarshal(colorsXml);
//
//                log.log(Level.INFO, "Loaded {0} colors from XML.", colors.getColors().size());
//                System.out.println("Loaded colors" + colors.getColors().size());
//
//            }
//            catch (IOException | JAXBException e)
//            {
//                log.log(Level.SEVERE, "Exception while attempting to unmarshall colors XML.", e);
//                System.out.println("Exception trying to unmarshall file.");
//                e.printStackTrace();
//            }
//        }
//
//        return colors;
//    }
//
//    public void setDevices(RosettaDevices devices) {
//
//        this.colors = colors;
//
//        try(final OutputStream colorsXml = new FileOutputStream(COLORS_FILE))
//        {
//
//            JAXBContext jxbContext = JAXBContext.newInstance(RosettaColors.class, RosettaColor.class);
//            Marshaller marshaller = jxbContext.createMarshaller();
//
//            marshaller.marshal(colors, colorsXml);
//
//            log.log(Level.INFO, "Wrote {0} colors to XML.", colors.getColors().size());
//
//        }
//        catch (IOException | JAXBException e)
//        {
//            log.log(Level.SEVERE, "Exception while attempting to marshal colors to XML.", e);
//            System.out.println("Exception trying to marshal file.");
//            e.printStackTrace();
//        }
//
//    }
}
