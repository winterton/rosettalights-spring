package com.rosetta.lights.utils;

import com.rosetta.lights.models.RosettaColor;
import com.rosetta.lights.models.RosettaColors;
import com.rosetta.lights.services.ColorMarshallService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.awt.*;

/**
 * Created by smithjw on 2/9/14.
 */
@Component
public class ColorHelper {

    private static ColorMarshallService colorMarshallService;

    @Autowired
    private ColorMarshallService autowiredMarshallService;

    public static Color stringToColor(String colorStr)
    {
        System.out.println("Pre processed str: " + colorStr);
        if(colorStr.length() == 7 && colorStr.charAt(0) == '#') {
            return Color.decode(colorStr);
        } else {
            //grab color from xml
            try{
                RosettaColors colors = ColorHelper.colorMarshallService.getColors();
                for (RosettaColor color : colors.getColors()) {
                    for(String name : color.getNames()) {
                        if(name.equalsIgnoreCase(colorStr)) {
                            return Color.decode(color.getHex());
                        }
                    }
                }
            } catch (Throwable t) {
                t.printStackTrace();
            }

        }

        return Color.decode("#00BCE4");
    }

    @PostConstruct
    public void init() {
        colorMarshallService = autowiredMarshallService;
    }
}
