package com.rosetta.lights.tasks;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.social.twitter.api.*;
import org.springframework.social.twitter.api.impl.TwitterTemplate;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by smithjw on 2/9/14.
 */
@Service
public class TwitterQueueBuilder extends AbstractQueueBuilder {

    final String consumerKey = "pLPx0DfdRL7bmXyHE2mKdA"; // The application's consumer key
    final String consumerSecret = "VJ28Kf6M3Xo93m99rF9WpqoDEu8FOHdjvEeBcrgWwX8"; // The application's consumer secret
    final String accessToken = "2313732662-ZEmMyZ5T0KwBMAb8xYb4eEupIldYlMUFgGqLkDO"; // The access token granted after OAuth authorization
    final String accessTokenSecret = "XCz4JkMUmeuC0Xr7fY5pNdiPYlrrRTxPrSdVQ1E91KO1o"; // The access token secret granted after OAuth authorization
    final Twitter twitter = new TwitterTemplate(consumerKey, consumerSecret, accessToken, accessTokenSecret);

    static long mostRecentId = 0;
    static List<StreamListener> listeners = new ArrayList<StreamListener>();
    static Stream userStream = null;


    @Deprecated
    public void retrieveRequestsFromSource()
    {
        List<Tweet> tweets = twitter.timelineOperations().getMentions();
        for(Tweet tweet : tweets) {
            parseTweet(tweet);
        }

    }

    public void beginListening()
    {
        if(listeners.isEmpty()){
            listeners.add(new StreamListener() {
                @Override
                public void onTweet(Tweet tweet) {
                    parseTweet(tweet);
                }

                @Override
                public void onDelete(StreamDeleteEvent streamDeleteEvent) {
                    //Do nothing
                }

                @Override
                public void onLimit(int i) {
                    System.out.println("RateLimited!");
                }

                @Override
                public void onWarning(StreamWarningEvent streamWarningEvent) {
                    //Do nothing
                }
            });
        }

        UserStreamParameters params = new UserStreamParameters().with(UserStreamParameters.WithOptions.USER).includeReplies(true);
        userStream = twitter.streamingOperations().user(params, listeners);
    }

    public void stopListening()
    {
        if(userStream != null)
        {
            System.out.println("Closing connection to user stream");
            userStream.close();
            userStream = null;
        }
    }

    public Boolean isListening()
    {
        if(userStream != null)
        {
            return true;
        }

        return false;
    }

    public void parseTweet(Tweet tweet)
    {
        if(tweet.getId() > mostRecentId) {
            mostRecentId = tweet.getId();

            System.out.println("NEW TWEET");
            System.out.println("[" + tweet.getId() + "] " + tweet.getText());
            int index = tweet.getText().lastIndexOf("#");
            if(index != -1 && tweet.getText().length() >= index+7) {
                String substring = tweet.getText().substring(index, index+7);

                try {
                    Integer.parseInt(tweet.getText().substring(index+1, index+7), 16);
                    boolean success = addColorToQueue(substring, String.valueOf(tweet.getId()));
                } catch (NumberFormatException e) {
                    // Do nothing
                }
            }

            for(HashTagEntity tag : tweet.getEntities().getHashTags()) {
                addColorToQueue(tag.getText(), String.valueOf(tweet.getId()));
            }
        } else {
            System.out.println("Duplicate/Stale tweet");
        }
    }

    public Twitter getTwitter() {
        return twitter;
    }
}
