package com.rosetta.lights.tasks;

import com.rosetta.lights.models.ColorRequestObject;
import com.rosetta.lights.models.ColorRequestQueue;
import com.rosetta.lights.utils.ColorHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Service;

/**
 * Created by smithjw on 2/9/14.
 */
@Service
public abstract class AbstractQueueBuilder implements  QueueBuilderInterface {

    @Autowired
    ColorRequestQueue<ColorRequestObject> queue;

    public boolean addColorToQueue(String color, String id) {
        ColorRequestObject obj = new ColorRequestObject(color);
        obj.setId(id);

        System.out.println(queue);

        if(!queue.contains(obj)) {
            queue.enqueue(obj);
            return true;
        }

        return false;
    }
}
