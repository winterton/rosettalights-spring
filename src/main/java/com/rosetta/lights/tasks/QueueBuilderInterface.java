package com.rosetta.lights.tasks;

/**
 * Created by smithjw on 2/9/14.
 */
public interface QueueBuilderInterface {

    public void retrieveRequestsFromSource();

}
