package com.rosetta.lights.models;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by jsmith20 on 3/17/14.
 */
@XmlRootElement(name = "device")
public class RosettaDevice {
    @XmlAttribute(name = "name")
    private String name;

    //@XmlElement("ip")
    private String  ipAddress;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }
}
