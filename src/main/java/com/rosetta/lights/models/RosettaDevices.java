package com.rosetta.lights.models;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

/**
 * Created by jsmith20 on 3/17/14.
 */
@XmlRootElement(name="devices")
@XmlAccessorType(XmlAccessType.FIELD)
public class RosettaDevices {

    @XmlElement(name="device")
    private List<RosettaDevice> deviceList;

    public List<RosettaDevice> getDeviceList() {
        return deviceList;
    }

    public void setDeviceList(List<RosettaDevice> deviceList) {
        this.deviceList = deviceList;
    }
}
