package com.rosetta.lights.models;

import javax.xml.bind.annotation.*;
import java.util.List;

/**
 * Created by smithjw on 2/19/14.
 */
@XmlRootElement(name = "color")
@XmlAccessorType (XmlAccessType.FIELD)
public class RosettaColor {

    @XmlAttribute(name = "hex")
    private String hex;

    @XmlElementWrapper(name = "names")
    @XmlElement(name = "name")
    private List<String> names;

    public List<String> getNames() {
        return names;
    }

    public void setNames(List<String> names) {
        this.names = names;
    }

    public String getHex() {

        return hex;
    }

    public void setHex(String hex) {
        this.hex = hex;
    }
}
