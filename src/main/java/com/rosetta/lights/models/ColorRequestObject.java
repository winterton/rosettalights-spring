package com.rosetta.lights.models;

import com.rosetta.lights.utils.ColorHelper;
import java.awt.*;

/**
 * Created by smithjw on 2/9/14.
 */
public class ColorRequestObject {

    private Integer r;
    private Integer g;
    private Integer b;

    private Color color;
    private String cId;



    public ColorRequestObject() {
        r = 0;
        g = 0;
        b = 0;

        color = Color.WHITE;
    }

    public ColorRequestObject(String c) {
        color = ColorHelper.stringToColor(c);
        r = color.getRed();
        g = color.getGreen();
        b = color.getBlue();

    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ColorRequestObject that = (ColorRequestObject) o;

        if (b != null ? !b.equals(that.b) : that.b != null) return false;
        if (cId != null ? !cId.equals(that.cId) : that.cId != null) return false;
        if (color != null ? !color.equals(that.color) : that.color != null) return false;
        if (g != null ? !g.equals(that.g) : that.g != null) return false;
        if (r != null ? !r.equals(that.r) : that.r != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = r != null ? r.hashCode() : 0;
        result = 31 * result + (g != null ? g.hashCode() : 0);
        result = 31 * result + (b != null ? b.hashCode() : 0);
        result = 31 * result + (color != null ? color.hashCode() : 0);
        result = 31 * result + (cId != null ? cId.hashCode() : 0);
        return result;
    }

    public String getR() {
        return r.toString();
    }

    public String getG() {
        return g.toString();
    }

    public String getB() {
        return b.toString();
    }

    public Color getColor() {
        return color;
    }

    public String getId() {
        return cId;
    }

    public void setId(String val) {
        cId = val;
    }

    public void setColor(Color c) {
        color = c;
    }
}
