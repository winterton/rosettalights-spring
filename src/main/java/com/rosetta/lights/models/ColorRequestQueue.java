package com.rosetta.lights.models;

import org.springframework.beans.factory.annotation.Qualifier;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.Queue;

/**
 * Created by smithjw on 2/9/14.
 */
@Qualifier("ColorRequestBean")
public class ColorRequestQueue<ColorRequestObject> implements Iterable<ColorRequestObject> {

    private LinkedList<ColorRequestObject> queue = new LinkedList<ColorRequestObject>();

    public ColorRequestQueue() {

    }

    public void enqueue(ColorRequestObject element)
    {
        queue.add(element);
    }

    public ColorRequestObject deque()
    {
        return queue.removeFirst();
    }

    public ColorRequestObject peek()
    {
       return queue.getFirst();
    }

    public int size()
    {
        return queue.size();
    }

    public boolean isEmpty()
    {
        return queue.isEmpty();
    }

    public LinkedList<ColorRequestObject> getQueue()
    {
        return (LinkedList<ColorRequestObject>)queue.clone();
    }

    public boolean contains(ColorRequestObject obj)
    {
        return queue.contains(obj);
    }

    @Override
    public Iterator<ColorRequestObject> iterator() {
        return queue.iterator();
    }


}
