package com.rosetta.lights.models;

import javax.xml.bind.annotation.*;
import java.util.List;

/**
 * Created by smithjw on 2/19/14.
 */
@XmlRootElement(name = "colors")
@XmlAccessorType(XmlAccessType.FIELD)
public class RosettaColors {

    @XmlElement(name = "color")
    private List<RosettaColor> colors;

    public List<RosettaColor> getColors() {
        return colors;
    }

    public void setColors(List<RosettaColor> colors) {
        this.colors = colors;
    }
}
